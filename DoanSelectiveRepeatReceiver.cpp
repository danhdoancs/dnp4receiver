/* 
 * File:   DoanSelectiveRepeatReceiver.cpp
 * Author: danh doan
 *
 * Created on November 20, 2015, 9:37 AM
 */

#include <cstdlib>
#include<stdio.h>
#include<string.h>    //strlen
#include<stdlib.h>    //strlen
#include<sys/socket.h>
#include<arpa/inet.h> //inet_addr
#include <unistd.h>    //write
#include <pthread.h> //for threading
#include <fcntl.h>
#include <sys/stat.h>

using namespace std;

#define BUFFER_SIZE 1024  //Max length of buffer
#define DELAY 200000 //200ms
#define PAYLOAD_SIZE 128

typedef struct {
    unsigned int seq;
    unsigned int ack;
    char payload[PAYLOAD_SIZE + 1];
    time_t time;
} frame;

void* messageListener(void *);
void die(char *s);
int onSenderData(char* msg);
int onSenderMessage(char* message);
int onSenderLogin(char *message);
int onSenderFile(char *body);
void createFrame(char* payload, int seq);
int checkOldestFrame(int indx);
void printStatisticalReport();
void clearSession();
void verifyOutputFile();
char* replace(char *st, char *orig, char *repl);

//global vars
struct sockaddr_in server;
int receiverSocket, i, slen = sizeof (server), status = 1, fileStatus = 1;
char buffer[BUFFER_SIZE];
char senderBuffer[BUFFER_SIZE];
char message[BUFFER_SIZE];
char* pMsg = message;
int currentAcceptedMsgIdx = 0, flagAuthentication = 0, flagSentNeAck = 0, windowSize, expectFrameNo = 0, currentIdx = 0;
FILE* outputFile;
frame** frameBuffers;
char frameBuffer[BUFFER_SIZE] = "";
int receivedPktNo = 0, retransmittedPktNo = 0, sentAckNo = 0, sentNeAck = 0;
char inputFileName[BUFFER_SIZE], outputFileName[BUFFER_SIZE];

/*
 * 
 */
int main(int argc, char** argv) {

    //check program parameters
    if (5 != argc) {
        fprintf(stderr, "Usage: %s <ip> <port> <input_file> <output_file>\n", argv[0]);
        exit(1);
    }
    srand(time(NULL));

    //create socket
    if ((receiverSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
        die("socket");
    }
    memset((char *) &server, 0, sizeof (server));
    server.sin_family = AF_INET;
    server.sin_port = htons(atoi(argv[2]));

    if (inet_aton(argv[1], &server.sin_addr) == 0) {
        fprintf(stderr, "inet_aton() failed\n");
        exit(1);
    }

    //loop if use want to receiver another files
    int flagFirstTransmit = 1;
    while (1) {
        //create a thread for handle sender message
        pthread_t thread_id;
        if (pthread_create(&thread_id, NULL, messageListener, NULL) < 0) {
            die("Could not create thread");
        }

        //authenticate
        while (flagAuthentication != 1) {
            printf("Username:");
            char username[BUFFER_SIZE], password[BUFFER_SIZE];
            scanf("%s", username);
            printf("Password:");
            scanf("%s", password);

            sprintf(message, "USERNAME.%s", username);
            //send username to sender
            if (sendto(receiverSocket, message, strlen(message), 0, (struct sockaddr *) &server, slen) == -1) {
                die("sendto()");
            }
            memset(message, 0, BUFFER_SIZE);
            sprintf(message, "PASSWORD.%s", password);
            //send username to sender
            if (sendto(receiverSocket, message, strlen(message), 0, (struct sockaddr *) &server, slen) == -1) {
                die("sendto()");
            }
            //wait 200ms for the next login
            usleep(DELAY);
        }

        //ask to start transfering
        char flagTransfer[BUFFER_SIZE] = "";
        do {
            memset(flagTransfer, 0, BUFFER_SIZE);
            fprintf(stdout, "Start transferring the file?(y/n): ");
            scanf("%s", flagTransfer);
        } while (strcmp(flagTransfer, "y") != 0 && strcmp(flagTransfer, "yes") != 0 && strcmp(flagTransfer, "Y") != 0);

        //create output file
        //ask output file name agains for second time and so on
  
        if (flagFirstTransmit) {
            strcpy(outputFileName, argv[4]);
        } else {
            memset(outputFileName, 0, BUFFER_SIZE);
            fprintf(stdout, "Output file name: ");
            scanf("%s", outputFileName);
        }
        outputFile = fopen(outputFileName, "w");
        if (flagFirstTransmit) {
            strcpy(inputFileName, argv[3]);
        } else {
            memset(inputFileName, 0, BUFFER_SIZE);
            fprintf(stdout, "Input file name: ");
            scanf("%s", inputFileName);
        }

        //send input file name
        memset(buffer, 0, BUFFER_SIZE);
        sprintf(buffer, "INPUT.%s", inputFileName);

        if (sendto(receiverSocket, buffer, strlen(buffer), 0, (struct sockaddr *) &server, slen) == -1) {
            die("sendto()");
        }
        //wait 200ms for the backend process
        usleep(DELAY);

        //ask input file again if it is wrong
        while (fileStatus != 1) {
            memset(inputFileName, 0, BUFFER_SIZE);
            fprintf(stdout, "Input file name: ");
            scanf("%s", inputFileName);
            //send input file name
            sprintf(buffer, "INPUT.%s", inputFileName);
            if (sendto(receiverSocket, buffer, strlen(buffer), 0, (struct sockaddr *) &server, slen) == -1) {
                die("sendto()");
            }
            //wait 400ms for the backend process
            usleep(DELAY);
        };

        //join two threads
        pthread_join(thread_id, NULL);
        fclose(outputFile);
        verifyOutputFile();
        printStatisticalReport();

        //ask user whether he want to receiver another file
        char flagAnotherTransfer[BUFFER_SIZE] = "";
        memset(flagAnotherTransfer, 0, BUFFER_SIZE);
        fprintf(stdout, "\nAnother file transfer?(y/n): ");
        scanf("%s", flagAnotherTransfer);
        if (strcmp(flagAnotherTransfer, "y") != 0 && strcmp(flagAnotherTransfer, "yes") != 0 && strcmp(flagAnotherTransfer, "Y") != 0) {
            break;
        } else {
            flagFirstTransmit = 0;
            clearSession();
        }
    }

    close(receiverSocket);
    return 0;
}

void die(char *s) {
    perror(s);
    exit(1);
}

int onSenderMessage(char* message) {
    //    printf("Received message: %s\n", message);
    char* type = strtok(message, ".");
    char* body = strtok(NULL, "");
    //skip if message is wrong format
    if (!type)
        return -1;

    if (strcmp(type, "LOGIN") == 0) {
        return onSenderLogin(body);
    } else if (strcmp(type, "FILE") == 0) {
        return onSenderFile(body);
    } else if (strcmp(type, "DATA") == 0) {
        return onSenderData(body);
    }
}

int onSenderData(char* msg) {
    int seqNo = atoi(strtok(msg, "."));
    char* payload = strtok(NULL, "\003");

    //random drop 10% Data packets
    double randomProb = (double) rand() / RAND_MAX;
    if (randomProb <= 0.1) {
        printf("Data frame %d is dropped!\n", seqNo);
        return 0;
    }
    receivedPktNo++;

    fprintf(stderr, "Received data frame %d: %s\n", seqNo, payload);
    //if it is the expecting frame, write to output
    //else, put into window buffer
    if (seqNo == expectFrameNo) {
        //        printf("Received expecting frame %d\n", seqNo);
        expectFrameNo++;
        //write data into output file
        fputs(payload, outputFile);
        //check old frame if they are now expecting frames
        checkOldestFrame(0);
        //send ACK for the lastest frame received
        memset(buffer, 0, BUFFER_SIZE);
        sprintf(buffer, "ACK.%d.1", expectFrameNo - 1);
        sentAckNo++;
        //send join request to server
        if (sendto(receiverSocket, buffer, strlen(buffer), 0, (struct sockaddr *) &server, slen) == -1) {
            die("sendto()");
        }
        fprintf(stderr, "Sending ack for frame %d\n", expectFrameNo - 1);
        //reset ne ack flag
        flagSentNeAck = 0;
    } else {
        //only handle new frames
        if (seqNo > expectFrameNo) {
            memset(buffer, 0, BUFFER_SIZE);
            if (!flagSentNeAck) {
                //send Negative ACK for expecting frame
                sprintf(buffer, "ACK.%d.-1", expectFrameNo);
                sentNeAck++;
                //send join request to server
                if (sendto(receiverSocket, buffer, strlen(buffer), 0, (struct sockaddr *) &server, slen) == -1) {
                    die("sendto()");
                }
                fprintf(stderr, "Sending negative ack for frame %d\n", expectFrameNo);
                flagSentNeAck = 1;
            } else { //already sent the neack, now send the lastest acceptable sequence frame
                sprintf(buffer, "ACK.%d.1", expectFrameNo - 1);
                sentAckNo++;
                //send ack
                if (sendto(receiverSocket, buffer, strlen(buffer), 0, (struct sockaddr *) &server, slen) == -1) {
                    die("sendto()");
                }
                fprintf(stderr, "Sending ack for frame %d\n", expectFrameNo - 1);
            }

            //skip if window is full
            if (currentIdx >= windowSize) {
                printf("The buffer list is full, drop unexpecting frame %d\n", seqNo);
                return 0;
            }

            createFrame(payload, seqNo);
        } else { // old ACK dropped
            //resend the latest ACK frame
            sprintf(buffer, "ACK.%d.1", expectFrameNo - 1);
            sentAckNo++;
            if (sendto(receiverSocket, buffer, strlen(buffer), 0, (struct sockaddr *) &server, slen) == -1) {
                die("sendto()");
            }
            fprintf(stderr, "Sending ack for frame %d\n", expectFrameNo - 1);
        }
    }

    return 1;
}

void* messageListener(void *) {
    //Get the socket descriptor
    //    struct sockaddr_in server;
    //loop to waiting for message from server
    while (1) {
        memset(senderBuffer, 0, BUFFER_SIZE);
        memset(buffer, 0, BUFFER_SIZE);
        socklen_t clientLen = sizeof (server);
        if ((recvfrom(receiverSocket, senderBuffer, BUFFER_SIZE, 0, (struct sockaddr *) &server,
                &clientLen)) > 0) {
            status = onSenderMessage(senderBuffer);
            //terminate the thread
            if (status < 0) {
                exit(1);
            } else if (status == 2) {
                int retrval = 100;
                pthread_exit(&retrval);
                //                return NULL;
                exit(1);
            }
        }
    }
    return 0;
}

int onSenderFile(char *body) {
    if (strcmp(body, "1") == 0) {
        //print success message
        fprintf(stderr, "The input file is ok!\n");
        fileStatus = 1;
        return 1;
    } else if (strcmp(body, "2") == 0) {
        //print success message
        fprintf(stderr, "Transfered the file successfully!\n");
        fileStatus = 1;
        return 2;
    } else {
        fprintf(stderr, "Failed to read the input file!\n");
        fileStatus = -1;
        return 0;
    }
}

int onSenderLogin(char *message) {
    //server return login success message
    if (strcmp(message, "0") != 0) {
        //set flag
        flagAuthentication = 1;
        //save window size
        windowSize = atoi(message);
        //print success message
        fprintf(stderr, "Login successfully!\n");
        fprintf(stderr, "Received window size: %d!\n", windowSize);
        //allocate memory
        frameBuffers = (frame**) malloc(windowSize);
        return 1;
    } else {
        //ask user the name and port of receiver
        fprintf(stderr, "Wrong username or password!\n");
        return 0;
    }
}

void createFrame(char* payload, int seq) {
    frame *pkt = new frame();
    strcpy(pkt->payload, payload);
    pkt->seq = seq;
    pkt->ack = 1;
    frameBuffers[currentIdx] = pkt;
    currentIdx = currentIdx + 1;
    //    printf("Frame %d is received!\n", pkt->seq);
}

int checkOldestFrame(int frameIdx) {
    //skip if there is no frame in buffer
    if (currentIdx < 1 || frameIdx >= currentIdx || !frameBuffers[frameIdx])
        return 0;
    //delete if it is the oldest frame and is expected
    if (expectFrameNo == frameBuffers[frameIdx]->seq) {
        //        printf("Process oldest expecting frame %d\n", frameBuffers[frameIdx]->seq);
        //write data into output file
        fputs(frameBuffers[frameIdx]->payload, outputFile);
        //delete the frame
        //        free(frameBuffers[frameIdx]);
        //if there is only one frame, just overwrite that frame
        if (frameIdx == currentIdx - 1) {
            //update current available Idx
            currentIdx--;
        } else {
            for (int i = frameIdx; i < currentIdx - 1; i++) {
                frameBuffers[i] = frameBuffers[i + 1];
            }
            //update current available Idx
            currentIdx--;
        }

        //update current expecting seq
        expectFrameNo++;
        //check the first element again
        return checkOldestFrame(0);
    }
    return checkOldestFrame(frameIdx + 1);
}

void printStatisticalReport() {
    int fildes = open(outputFileName, O_RDWR);
    struct stat statBuffer;
    fstat(fildes, &statBuffer);
    //get file size
    int fileSize = statBuffer.st_size;
    //get file creation date
    char creationDateStr[BUFFER_SIZE];
    struct tm* clock;
    clock = gmtime(&(statBuffer.st_mtime));
    sprintf(creationDateStr, "%d-%d-%d %d:%d:%d", clock->tm_year + 1900, clock->tm_mon + 1, clock->tm_mday, clock->tm_hour, clock->tm_min, clock->tm_sec);

    printf("\nStatistical Report:\n");
    printf("Input file: %s (%d bytes)\n", inputFileName, fileSize);
    printf("Output file: %s (%d bytes)\n", outputFileName, fileSize);
    printf("Output file creation date: %s\n", creationDateStr);
    printf("Number of packets received: %d\n", receivedPktNo);
    printf("Number of ACK packets sent: %d\n", sentAckNo);
    printf("Number of Negative ACK pakcets sent: %d\n", sentNeAck);
}

void verifyOutputFile() {
    //check if the file is correct or not
    printf("\nVerifying output file:\n");
    memset(buffer, 0, BUFFER_SIZE);
    sprintf(buffer, "diff -s %s %s", inputFileName, outputFileName);
    system(buffer);
}

void clearSession() {
    memset(buffer, 0, BUFFER_SIZE);
    memset(senderBuffer, 0, BUFFER_SIZE);
    memset(message, 0, BUFFER_SIZE);
    currentAcceptedMsgIdx = 0;
    currentIdx = 0;
    flagSentNeAck = 0;
    retransmittedPktNo = 0;
    expectFrameNo = 0;
    receivedPktNo = 0;
    retransmittedPktNo = 0;
    sentAckNo = 0;
    sentNeAck = 0;
}

char* replace(char *st, char *orig, char *repl) {
    static char buffer[BUFFER_SIZE];
    char *ch;
    if (!(ch = strstr(st, orig)))
        return st;
    strncpy(buffer, st, ch - st);
    buffer[ch - st] = 0;
    sprintf(buffer + (ch - st), "%s%s", repl, ch + strlen(orig));
    return buffer;
}