How to Get a Girlfriend and Keep Her 
By Nick Savoy

Getting a girlfriend is an important business in a man's life. Unless you are famous, extremely rich or a male model you've probably found that it's not easy to make a beautiful woman your girlfriend. You may believe fate will throw the woman of your dreams into your path. It's possible, but you could be waiting a long time. As with most things in life good things tend to come to those who actually seek them.

Here are a few pointers to get you on your way to finding a girlfriend...

Firstly, if you want to get a girlfriend you have to actually meet girls. Look for ways to get them in your life. Go out, meet people, join clubs, and get involved in activities where you're likely to meet girls. Make friends with everyone, but especially people who know a lot of women. Once you gain a wider social circle you will find yourself coming into contact with more girls. You'll also be better equipped in social situations.

Having good social skills and confidence are essential if you are going to approach women in bars and nightclubs. Girls you meet in these situations will be more critical of how you come across - they are being hit on by a lot of guys and will make fast, sometimes harsh assessments. You can learn all about approaching girls and how you should steer your conversations in our dating bible Magic Bullets.

If you can get a girl on a date it means she's interested in you. Mess it up and she won't be for much longer. Don't make the date too formal (dinner = bad idea, cinema = worst) and try to make physical contact with her as much as possible. Ideally go somewhere that has some form of entertainment that can take the pressure off your conversation and go to a few different venues; it will make her feel like she's known you for longer.

Moving on to the more general guidelines, you've probably heard that women want a badboy... it's not completely true. Women want a strong man - someone with his own opinions, who stands up for himself and doesn't take any crap - least of all from her. You don't need to be an asshole, but try being a little selfish. Don't be afraid to cancel plans if they don't suit you and don't do anything a selfish man would think was too much of a hassle.

At all times you have to remember never to make a girl feel like you need her. Being needy is a one-way ticket to nights at home with microwave meals for one. Stay away from emotional texts like "So great to meet you, haven't stopped thinking about you since..." or "I know it's too early to say this, but I really feel connected to you in a deep way." It's okay to text stuff like this when she's your girlfriend, and she will think it's cute. But do it when you've just met her and she'll think you're a weirdo.

Don't bring up the "are we officially girlfriend and boyfriend" conversation before it's absolutely necessary; ideally wait until she starts hinting. If she doesn't, then casually say something along the lines of "We never discussed this... but are we meant to be seeing other people?" If you've taken your time and you've been seeing a lot of each other then you shouldn't get too many surprises at this point.

If you can steer your way through all of that then congratulations - you have a girlfriend, regular sex, and maybe a dream of eternal happiness. It's not a process that's easy so don't get down when you get it wrong. Learn your lessons, improve, and move onto the next lucky lady.

Source: http://www.lovesystems.com/dating-advice/miscellaneous/get-a-girlfriend